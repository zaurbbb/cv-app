module.exports = {
  // collectCoverage: true,
  collectCoverageFrom: [
    "src/**/*.{js,jsx}",
    "!src/redux/index.js",
    "!src/app/index.js",
    "!src/helpers/renderTestApp.jsx",
    "!src/main.jsx",
    "!.jest/mocks/file-mock.js",
    "!src/services/server.js",
  ],
  "coverageThreshold": {
    "global": {
      "branches": 40,
      "functions": 40,
      "lines": 40,
      "statements": 40
    }
  },
  testEnvironment: "jest-environment-jsdom",
  setupFilesAfterEnv: ["<rootDir>/.jest/setup-tests.js"],
  moduleNameMapper: {
    "\\.(gif|ttf|eot|svg|png)$": "<rootDir>/.jest/mocks/file-mock.js",
    "\\.(css|less|sass|scss)$": "identity-obj-proxy",
  }
};

