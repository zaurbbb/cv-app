import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";

import { Provider } from "react-redux";

import { PersistGate } from "redux-persist/integration/react";
import {
  persistor,
  createReduxStore,
} from "./redux";

import { App } from "./app";

import "./styles/main.scss";
import { makeServer } from "./services/server";

makeServer();

ReactDOM.createRoot(document.getElementById("root"))
  .render(
    <BrowserRouter>
      <Provider store={createReduxStore()}>
        <PersistGate persistor={persistor}>
          <App />
        </PersistGate>
      </Provider>
    </BrowserRouter>,
  );
