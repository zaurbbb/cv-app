import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Loader() {
  return (
    <FontAwesomeIcon
      className="loader"
      icon="sync-alt"
      size="4x"
    />
  );
}

export default Loader;
