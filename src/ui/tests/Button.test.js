import { screen } from "@testing-library/react";

import { renderTestApp } from "../../helpers/renderTestApp";

describe("ui/Button tests", () => {
  test("successfully renders button on panel component", () => {
    renderTestApp({
      route: "/profile",
      initialState: {},
    });

    const button = screen.getAllByTestId("button")[0];
    expect(button).toHaveTextContent(/Go back/i);
  });
});

