import React from "react";


function Error({
  // component props
  message,
}) {
  return (
    <p className="error">
      {message}
    </p>
  );
}

export default Error;
