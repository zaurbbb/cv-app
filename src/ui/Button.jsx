import React from "react";

function Button({
  // component props
  icon,
  text,
  onClick,
  type,
  disabled,
  isDisabled,
}) {
  // constants
  const classNameWithDisable = !disabled ? "button button--active" : "button button--disabled";
  const classnameWithoutDisable = "button";
  const className = !isDisabled ? classnameWithoutDisable : classNameWithDisable;
  return (
    <button
      className={className}
      type={type}
      onClick={onClick}
      data-testid="button"
    >
      {icon}
      {text}
    </button>
  );
}

export default Button;
