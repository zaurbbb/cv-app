import React from "react";

function PhotoBox({
  // component props
  block,
  name,
  title,
  description,
  avatar,
}) {
  // constants
  const avatarClassName = `${block}__avatar`;
  const fullnameClassName = `${block}__fullname`;
  const positionClassName = `${block}__position`;
  const descriptionClassName = `${block}__description`;

  // display components
  const displayLargeName = title
    && description
    && (
      <h1 className={fullnameClassName}>
        {name}
      </h1>
    );
  const displaySmallName = !title
    && !description
    && (
      <h4 className={fullnameClassName}>
        {name}
      </h4>
    );
  const displayTitle = title && (
    <h3 className={positionClassName}>
      {title}
    </h3>
  );
  const displayDescription = description && (
    <p className={descriptionClassName}>
      {description}
    </p>
  );

  return (
    <>
      <img
        className={avatarClassName}
        src={avatar}
        alt="user avatar"
      />
      {displayLargeName}
      {displaySmallName}
      {displayTitle}
      {displayDescription}
    </>
  );
}

export default PhotoBox;
