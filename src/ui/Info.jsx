import React from "react";

function Info({
  // component props
  title,
  text,
  block,
}) {
  // constants
  const infoClassName = block ? `${block}__info info` : "info";
  const titleClassName = block ? `${block}__title info__title` : "info__title";
  const textClassName = block ? `${block}__text info__text` : "info__text";

  // display components
  const displayTitle = title && (
    <span className={titleClassName}>
      {title}
    </span>
  );

  return (
    <p className={infoClassName}>
      {displayTitle}
      <span className={textClassName}>
        {text}
      </span>
    </p>
  );
}

export default Info;
