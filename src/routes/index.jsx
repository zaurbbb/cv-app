import HomePage from "../pages/HomePage";
import ProfilePage from "../pages/ProfilePage";

const routes = [
  { path: "/", component: <HomePage /> },
  { path: "/profile", component: <ProfilePage /> },
];

export { routes };
