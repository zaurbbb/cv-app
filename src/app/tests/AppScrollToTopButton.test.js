import {
  screen,
  waitFor,
} from "@testing-library/react";

import { renderTestApp } from "../../helpers/renderTestApp";
import userEvent from "@testing-library/user-event";

userEvent.setup();

describe("ui/Button tests", () => {
  test("successfully renders button on panel component", () => {
    renderTestApp({
      route: "/profile#feedbacks",
      initialState: {},
    });

    const mockScrollTo = jest.fn();
    window.HTMLElement.prototype.scrollTo = mockScrollTo;

    const button = screen.getByTestId("scroll-to-top-button");
    expect(button).toBeInTheDocument();
    userEvent.click(button);
    waitFor(() => {
      expect(mockScrollTo).toHaveBeenCalledWith({
        top: 0,
        behavior: "smooth",
      });
    });
  });
});

