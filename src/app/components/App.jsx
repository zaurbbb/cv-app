import React, { useState } from "react";
import { useLocation } from "react-router-dom";

import { library } from "@fortawesome/fontawesome-svg-core";
import { fab } from "@fortawesome/free-brands-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import { far } from "@fortawesome/free-regular-svg-icons";

import AppRouter from "./AppRouter";
import Panel from "../../components/Panel";
import AppScrollToTopButton from "./AppScrollToTopButton";

function App() {
  // constants
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);

  // constants
  const { pathname } = useLocation();
  const appClassName = pathname === "/" ? "app app--home" : "app";
  const panelClassName = isSidebarOpen ? "panel" : "panel closed";
  const mainClassName = isSidebarOpen ? "main-content" : "main-content expanded";

  // handlers
  const handleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  // display components
  const displayAside = pathname === "/profile" && (
    <Panel
      className={panelClassName}
      handleSidebar={handleSidebar}
    />
  );
  const displayScrollToTopButton = pathname !== "/" && (
    <AppScrollToTopButton />
  );


  return (
    <div className={appClassName}>
      {displayAside}
      <main
        className={mainClassName}
        data-testid="main"
      >
        <AppRouter />
      </main>
      {displayScrollToTopButton}
    </div>
  );
}

export default App;
library.add(fab, fas, far);
