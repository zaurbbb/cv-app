import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function AppScrollToTopButton() {
  // handlers
  const scrollToTop = () => {
    const mainElement = document.querySelector("main");
    mainElement.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <button
      className="scroll-to-top-button"
      onClick={scrollToTop}
      data-testid="scroll-to-top-button"
    >
      <FontAwesomeIcon icon="chevron-up" />
    </button>
  );
}

export default AppScrollToTopButton;
