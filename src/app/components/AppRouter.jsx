import React from "react";
import {
  Route,
  Routes,
} from "react-router-dom";

import { routes } from "../../routes";

function AppRouter() {
  return (
    <Routes>
      {/* iterate routes */}
      {routes.map((route) => (
        <Route
          key={route.path}
          path={route.path}
          element={route.component}
        />
      ))}
    </Routes>
  );
}

export default AppRouter;
