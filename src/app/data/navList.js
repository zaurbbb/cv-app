export const navList = [
  {
    name: "About me",
    path: "#about-me",
    icon: "user",
  },
  {
    name: "Education",
    path: "#Education",
    icon: "graduation-cap",
  },
  {
    name: "Experience",
    path: "#experience",
    icon: "pen",
  },
  {
    name: "Skills",
    path: "#skills",
    icon: "gem",
  },
  {
    name: "Portfolio",
    path: "#catalog",
    icon: "suitcase",
  },
  {
    name: "Contacts",
    path: "#contacts",
    icon: "location-arrow",
  },
  {
    name: "Feedbacks",
    path: "#feedbacks",
    icon: "comment",
  }
];
