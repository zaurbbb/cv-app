import React from "react";

import {
  ErrorMessage,
  Field,
  Form,
  Formik,
} from "formik";
import { useDispatch } from "react-redux";

import {
  addSkill,
  fetchSkills,
} from "../redux";

import Button from "../ui/Button";


function AddSkill() {
  // constants
  const dispatch = useDispatch();
  const initialValues = {
    name: "",
    range: "",
  };

  // validation
  const validate = (values) => {
    const {
      name,
      range,
    } = values;
    const errors = {};

    if (!name) errors.name = "Skill name is required field";
    if (!range) errors.range = "Skill range is required field";
    if (
      typeof Number(range) !== "number"
      || isNaN(Number(range))
    ) errors.range = "Skill range must be a `number` type";
    if (Number(range) < 10) errors.range = "Skill range must be greater than or equal to 10";
    if (Number(range) > 100) errors.range = "Skill range must be less than or equal to 100";

    return errors;
  };

  // submit
  const onSubmit = ({
    name,
    range,
  }) => {
    dispatch(addSkill({
      name,
      range,
    }));
    dispatch(fetchSkills());
  };

  return (
    <div className="skills__edit">
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validate={validate}
      >
        {({
          values,
          errors,
          isValid,
        }) => (
          <Form className="skills__form">
            <div className="skills__form-group">
              <div className="skills__input-group">
                <label
                  className="skills__label"
                  htmlFor="name"
                >
                  Skill name:
                </label>
                <Field
                  type="text"
                  name="name"
                  id="name"
                  className={`skills__input ${
                    errors.name ? "skills__input--error" : ""
                  }`}
                  placeholder="Enter skill name"
                />
              </div>
              <ErrorMessage
                component="div"
                name="name"
                className="skills__error"
              />
            </div>
            <div className="skills__form-group">
              <div className="skills__input-group">
                <label
                  className="skills__label"
                  htmlFor="range"
                >
                  Skill range:
                </label>
                <Field
                  type="text"
                  name="range"
                  id="range"
                  className={`skills__input ${
                    errors.range ? "skills__input--error" : ""
                  }`}
                  placeholder="Enter skill range"
                />
              </div>
              <ErrorMessage
                component="div"
                name="range"
                className="skills__error"
              />
            </div>
            <Button
              text="Add skill"
              type="submit"
              disabled={!isValid || !values.name || !values.range}
              isDisabled={true}
            />
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default AddSkill;
