import React from "react";
import Info from "../ui/Info";

function Feedback({
  // component props
  data,
}) {
  // display components
  const displayFeedback = data.map((item, index) => {
    const { feedback } = item;
    const {
      photoUrl,
      name,
      citeUrl,
    } = item.reporter;
    const trimmedCiteUrl = citeUrl.replace(/^https?:\/\/www./i, "");

    return (
      <div
        key={index}
        className="feedback__item"
      >
        <Info
          text={feedback}
          block="feedback"
        />
        <div className="feedback__reporter">
          <img
            className="feedback__photo"
            src={photoUrl}
            alt="user"
          />
          <p className="feedback__name">
            {name},
            {" "}
            <a
              href={citeUrl}
              className="feedback__cite"
            >
              {trimmedCiteUrl}
            </a>
          </p>
        </div>
      </div>
    );
  });

  return (
    <div className="feedback">
      {displayFeedback}
    </div>
  );
}

export default Feedback;
