function Box({
  // component props
  title,
  content,
}) {
  return (
    <div className="box">
      <h3
        className="box__title title"
        data-testid="box-title"
      >
        {title}
      </h3>
      <p className="box__content">
        {content}
      </p>
    </div>
  );
}

export default Box;
