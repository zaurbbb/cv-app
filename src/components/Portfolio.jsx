import React, {
  useEffect,
  useRef,
  useState,
} from "react";
import Isotope from "isotope-layout";

import Photo1 from "/images/portfolioItem1.png";
import Photo2 from "/images/portfolioItem2.png";

function Portfolio() {
  // state
  const [filterKey, setFilterKey] = useState("*");

  // constants
  const isotope = useRef();
  const filterAll = "*";
  const filterUI = "ui";
  const filterCode = "code";
  const portfolioTabs = [
    {
      text: "All",
      filterType: filterAll,
    },
    {
      text: "Code",
      filterType: filterCode,
    },
    {
      text: "UI",
      filterType: filterUI,
    },
  ];
  const portfolioItems = [
    {
      id: 1,
      image: Photo1,
      type: filterUI,
    },
    {
      id: 2,
      image: Photo2,
      type: filterCode,
    },
    {
      id: 3,
      image: Photo1,
      type: filterUI,
    },
    {
      id: 4,
      image: Photo2,
      type: filterCode,
    },
  ];

  // initialize an Isotope object with configs
  useEffect(() => {
    isotope.current = new Isotope(".portfolio__items", {
      itemSelector: ".portfolio__item",
      layoutMode: "fitRows",
    });
    return () => isotope.current.destroy();
  }, []);
  // handling filter key change
  useEffect(() => {
    filterKey === "*"
      ? isotope.current.arrange({ filter: filterAll })
      : isotope.current.arrange({ filter: `.${filterKey}` });
  }, [filterKey]);

  // handlers
  const handleFilterKeyChange = key => () => setFilterKey(key);

  // display components
  const displayPortfolioTabs = portfolioTabs
    .map(({
      text,
      filterType,
    }) => {
      // constants
      const className = filterKey === filterType ? "portfolio__tab portfolio__tab--active" : "portfolio__tab";

      return (
        <li
          key={filterType}
          className={className}
          onClick={handleFilterKeyChange(filterType)}
        >
          {text}
        </li>
      );
    });
  const displayPortfolioItems = portfolioItems
    .map(({
      id,
      image,
      type,
    }) => {
      // constants
      const className = `portfolio__item ${type}`;
      const title = "Some text";
      const description = "Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis";
      const highlight = "View resource";

      return (
        <li
          key={id}
          className={className}
        >
          <div className="portfolio__card">
            <div className="portfolio__card-content">
              <img
                className="portfolio__image"
                src={image}
                alt="portfolio item"
              />
              <div className="portfolio__hover-content">
                <h4 className="portfolio__title">
                  {title}
                </h4>
                <p className="portfolio__description">
                  {description}
                </p>
                <span className="portfolio__highlight">
                  {highlight}
                </span>
              </div>
            </div>
          </div>
        </li>
      );
    });

  return (
    <div className="portfolio">
      <ul className="portfolio__tabs">
        {displayPortfolioTabs}
      </ul>
      <hr />
      <ul className="portfolio__items">
        {displayPortfolioItems}
      </ul>
    </div>
  );
}

export default Portfolio;
