import Info from "../ui/Info";
import Loader from "../ui/Loader";
import Error from "../ui/Error";

function TimeLine({
  // component props
  state,
}) {
  // constants
  const {
    educations,
    isLoading,
    error,
  } = state;

  if (isLoading) return <Loader />;
  if (error) return <Error message={error} />;

  // display components
  const displayTimeLine = educations.map(({
    title,
    text,
    date,
  }) => (
    <li
      key={date}
      className="timeline__item"
    >
      <div className="timeline__date">
        {date}
      </div>
      <div className="timeline__event">
        <Info
          block="timeline"
          title={title}
          text={text}
        />
      </div>
    </li>
  ));

  return (
    <div className="timeline">
      <ul className="timeline__list">
        {displayTimeLine}
      </ul>
    </div>
  );
}

export default TimeLine;
