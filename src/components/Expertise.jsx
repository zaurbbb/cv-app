import React from "react";

function Expertise({
  // component props
  data,
}) {
  // display component
  const displayData = data.map((item, index) => {
    // constants
    const { date } = item;
    const {
      company,
      job,
      description,
    } = item.info;
    return (
      <div
        key={index}
        className="expertise__item"
      >
        <div className="expertise__company">
          <span className="expertise__title">
            {company}
          </span>
          <span className="expertise__subtitle">
            {date}
          </span>
        </div>
        <div className="expertise__position">
        <span className="expertise__title">
          {job}
        </span>
          <div className="expertise__description">
            {description}
          </div>
        </div>
      </div>
    );
  });

  return (
    <div className="expertise">
      {displayData}
    </div>
  );
}

export default Expertise;
