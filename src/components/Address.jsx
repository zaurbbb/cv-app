import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

function Address() {
  const data = [
    {
      icon: "phone",
      title: "500 342 242",
      subtitle: null,
    },
    {
      icon: "envelope",
      title: "office@kamsolutions.pl",
      subtitle: null,
    },
    {
      icon: "fa-brands fa-twitter",
      title: "Twitter",
      subtitle: "https://twitter.com/wordpress",
    },
    {
      icon: "fa-brands fa-facebook",
      title: "Facebook",
      subtitle: "https://www.facebook.com/facebook",
    },
    {
      icon: "fa-brands fa-skype",
      title: "Skype",
      subtitle: "kamsolutions.pl",
    },
  ];

  const displayAddress = data.map((item, index) => {
    // constants
    const {
      icon,
      title,
      subtitle,
    } = item;

    // display components
    const displayPhoneTitle = icon === "phone" && (
      <a
        className="address__title"
        href={`tel:+${title}`}
      >
        {title}
      </a>
    );
    const displayEmailTitle = icon === "envelope" && (
      <a
        className="address__title"
        href={`mailto:${title}`}
      >
        {title}
      </a>
    );
    const displayTwitterTitle = icon === "fa-brands fa-twitter" && (
      <span className="address__title">
        {title}
      </span>
    );
    const displayTwitterSubtitle = icon === "fa-brands fa-twitter" && (
      <a
        className="address__subtitle"
        href={subtitle}
      >
        {subtitle}
      </a>
    );
    const displayFacebookTitle = icon === "fa-brands fa-facebook" && (
      <span className="address__title">
        {title}
      </span>
    );
    const displayFacebookSubtitle = icon === "fa-brands fa-facebook" && (
      <a
        className="address__subtitle"
        href={subtitle}
      >
        {subtitle}
      </a>
    );
    const displaySkypeTitle = icon === "fa-brands fa-skype" && (
      <span className="address__title">
        {title}
      </span>
    );
    const displaySkypeSubtitle = icon === "fa-brands fa-skype" && (
      <a
        className="address__subtitle"
        href={`skype:${subtitle}`}
      >
        {subtitle}
      </a>
    );

    return (
      <div
        key={index}
        className="address__item"
      >
        <FontAwesomeIcon
          className="address__icon"
          icon={icon}
          size="2x"
        />
        <p className="address__text">
          {displayPhoneTitle}
          {displayEmailTitle}
          {displayTwitterTitle}
          {displayFacebookTitle}
          {displaySkypeTitle}
          <br/>
          {displayTwitterSubtitle}
          {displayFacebookSubtitle}
          {displaySkypeSubtitle}
        </p>
      </div>
    );
  });
  return (
    <div className="address">
      {displayAddress}
    </div>
  );
}

export default Address;
