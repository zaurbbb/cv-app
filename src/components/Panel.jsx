import React from "react";
import { Link } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Navigation from "./Navigation";
import Button from "../ui/Button";

import Avatar from "/images/greetingAvatar.png";
import PhotoBox from "../ui/PhotoBox";

function Panel({
  // component props
  className,
  handleSidebar,
}) {
  return (
    <aside className={className}>

      <div className="panel__content">

        <div className="panel__profile">
          <PhotoBox
            block="panel"
            name="John Doe"
            avatar={Avatar}
          />
        </div>

        <Navigation />
      </div>

      <Link
        className="panel__go-back panel__go-back--desktop panel__wrapper"
        to="/"
      >
        <Button
          icon={<FontAwesomeIcon icon="chevron-left" />}
          text="Go Back"
        />
      </Link>

      <Link
        className="panel__go-back panel__go-back--mobile"
        to="/"
      >
        <div className="panel__wrapper">
          <FontAwesomeIcon icon="chevron-left" />
        </div>
      </Link>

      <div
        className="panel__hamburger"
        onClick={handleSidebar}
      >
        <FontAwesomeIcon icon="bars" />
      </div>
    </aside>
  );
}

export default Panel;
