import React from "react";
import {
  fireEvent,
  render,
  waitFor,
} from "@testing-library/react";
import { useDispatch } from "react-redux";

import AddSkill from "../AddSkill";

// Mocking redux dispatch
jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));

// Mocking Redux actions
jest.mock("../../redux", () => ({
  addSkill: jest.fn(),
  fetchSkills: jest.fn(),
}));

describe("components/AddSkill tests", () => {
  let dispatchMock;

  beforeEach(() => {
    // Reset mock before each test
    dispatchMock = jest.fn();
    useDispatch.mockReturnValue(dispatchMock);
  });

  it("should display error messages for invalid input", async () => {
    const { getByLabelText, getByText, queryByText } = render(<AddSkill />);
    const nameInput = getByLabelText("Skill name:");
    const rangeInput = getByLabelText("Skill range:");
    const addButton = getByText("Add skill");

    fireEvent.change(nameInput, { target: { value: "" } });
    fireEvent.change(rangeInput, { target: { value: "" } });
    fireEvent.click(addButton);

    // Validate error messages
    await waitFor(() => {
      expect(queryByText((content, element) => {
        return element && element.textContent.startsWith("Skill name is required");
      })).not.toBeNull();
    });

    fireEvent.change(rangeInput, { target: { value: "5" } });
    fireEvent.click(addButton);

    // Validate error message for invalid range
    await waitFor(() => {
      expect(queryByText((content, element) => {
        return element && element.textContent.startsWith("Skill range must be greater");
      })).toBeTruthy();
    });

    fireEvent.change(rangeInput, { target: { value: "200" } });
    fireEvent.click(addButton);

    // Validate error message for range exceeding limit
    await waitFor(() => {
      expect(queryByText((content, element) => {
        return element && element.textContent.startsWith("Skill range must be less");
      })).toBeTruthy();
    });
  });

  it("should render the form inputs", () => {
    const { getByLabelText } = render(<AddSkill />);

    expect(getByLabelText("Skill name:")).toBeInTheDocument();
    expect(getByLabelText("Skill range:")).toBeInTheDocument();
  });

  it("should display error messages for invalid input", async () => {
    const { getByLabelText, getByText, queryByText } = render(<AddSkill />);
    const nameInput = getByLabelText("Skill name:");
    const rangeInput = getByLabelText("Skill range:");
    const addButton = getByText("Add skill");

    fireEvent.change(nameInput, { target: { value: "" } });
    fireEvent.change(rangeInput, { target: { value: "" } });
    fireEvent.click(addButton);

    // Validate error messages
    await waitFor(() => {
      expect(queryByText((content, element) => {
        return element && element.textContent.startsWith("Skill name is required");
      })).toBeNull();

      expect(queryByText((content, element) => {
        return element && element.textContent.startsWith("Skill range is required");
      })).toBeNull();
    });

    fireEvent.change(rangeInput, { target: { value: "5" } });
    fireEvent.click(addButton);

    // Validate error message for invalid range
    await waitFor(() => {
      expect(queryByText((content, element) => {
        return element && element.textContent.startsWith("Skill range must be greater");
      })).toBeInTheDocument();
    });

    fireEvent.change(rangeInput, { target: { value: "200" } });
    fireEvent.click(addButton);

    // Validate error message for range exceeding limit
    await waitFor(() => {
      expect(queryByText((content, element) => {
        return element && element.textContent.startsWith("Skill range must be less");
      })).toBeNull();
    });
  });
});
