import React from "react";
import {
  Link,
  useLocation,
} from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { navList } from "../app/data/navList";

function Navigation() {
  // constants
  const { hash } = useLocation();

  // display components
  const displayNavList = navList
    .map(({
      name,
      path,
      icon,
    }) => (
      <a
        key={name}
        className={hash === path ? "nav__item nav__item--active" : "nav__item"}
        href={path}
      >
        <FontAwesomeIcon icon={icon} />
        <span className="nav__text">
          {name}
        </span>
      </a>
    ));

  return (
    <nav className="nav">
      {displayNavList}
    </nav>
  );
}

export default Navigation;
