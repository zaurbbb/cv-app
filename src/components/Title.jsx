import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import Button from "../ui/Button";

function Title({
  // component props
  text,
  button,
  handleClick,
}) {
  return (
    <div className="title">
      <h3 className="title__text">
        {text}
      </h3>
      {button && (
        <Button
          icon={<FontAwesomeIcon icon="fas fa-edit" />}
          text="Open edit"
          onClick={handleClick}
        />
      )}
    </div>
  );
}

export default Title;
