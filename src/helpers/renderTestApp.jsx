import React from "react";
import { MemoryRouter } from "react-router-dom";

import { Provider } from "react-redux";
import { render } from "@testing-library/react";

import { createReduxStore } from "../redux";

import { App } from "../app";

export const renderTestApp = (options, component = null) => {
  return render(
    <Provider store={createReduxStore(options?.initialState)}>
      <MemoryRouter initialEntries={[options.route]}>
        <App />
        {component}
      </MemoryRouter>
    </Provider>,
  );
};
