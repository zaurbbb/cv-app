export {
  createReduxStore,
  persistor,
} from "./store/index.js";

export { fetchEducations } from "./store/asyncThunks/educationThunks";
export { fetchSkills } from "./store/asyncThunks/skillsThunks";
export { addSkill } from "./store/asyncThunks/skillsThunks";
