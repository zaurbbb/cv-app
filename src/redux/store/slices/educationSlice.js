import { createSlice } from "@reduxjs/toolkit";
import { fetchEducations } from "../asyncThunks/educationThunks";

const educationSlice = createSlice({
  name: "toolkit",
  initialState: {
    educations: [],
    isLoading: false,
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchEducations.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchEducations.fulfilled, (state, action) => {
        state.isLoading = false;
        state.educations = action.payload;
        state.error = null;
      })
      .addCase(fetchEducations.rejected, (state) => {
        state.isLoading = false;
        state.error = "Something went wrong; please review your server connection!";
      });
  },
});

export default educationSlice.reducer;
export const { setEducations } = educationSlice.actions;
