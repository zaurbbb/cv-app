import { createSlice } from "@reduxjs/toolkit";
import { fetchSkills } from "../asyncThunks/skillsThunks";

const skillSlice = createSlice({
  name: "toolkit",
  initialState: {
    skills: [],
    isLoading: false,
    error: null,
  },
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchSkills.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(fetchSkills.fulfilled, (state, action) => {
        state.isLoading = false;
        state.skills = action.payload;
        state.error = null;
      })
      .addCase(fetchSkills.rejected, (state) => {
        state.isLoading = false;
        state.error = "Something went wrong; please review your server connection!";
      });
  },
});

export default skillSlice.reducer;
export const { addSkill } = skillSlice.actions;
