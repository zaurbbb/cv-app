import {
  combineReducers,
  configureStore,
} from "@reduxjs/toolkit";
import {
  persistReducer,
  persistStore,
} from "redux-persist";
import storage from "redux-persist/lib/storage";
import skillsSlice from "./slices/skillSlice.js";
import educationsSlices from "./slices/educationSlice";

const rootReducer = combineReducers({
  skill: skillsSlice,
  education: educationsSlices,
});

const persistConfig = {
  key: "root",
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

function createReduxStore(initialState = {}) {
  return configureStore({
    reducer: persistedReducer,
    preloadedState: initialState,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
      serializableCheck: false,
    }),
  });
}

const persistor = persistStore(createReduxStore());

export {
  createReduxStore,
  persistor,
}
