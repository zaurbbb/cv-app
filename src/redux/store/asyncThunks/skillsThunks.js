import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../../../api";

const fetchSkills = createAsyncThunk(
  "users/fetchSkills", // отображается в dev tools и должно быть уникально у каждого Thunk
  async () => {
    const response = await API.get("/skills");
    console.log("response.data", response.data);
    return response.data;
  },
);

const addSkill = createAsyncThunk(
  "users/addSkill", // отображается в dev tools и должно быть уникально у каждого Thunk
  async (skill) => {
    console.log("skill", skill);
    const response = await API.post("/skills", skill);
    return response.data;
  },
);
export {
  fetchSkills,
  addSkill,
};
