import { createAsyncThunk } from "@reduxjs/toolkit";
import { API } from "../../../api";

export const fetchEducations = createAsyncThunk(
  "users/fetchEducations", // отображается в dev tools и должно быть уникально у каждого Thunk
  async () => {
    const response = await API.get("/educations");
    return response.data;
  }
);
