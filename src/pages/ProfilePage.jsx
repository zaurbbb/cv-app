import React from "react";

import Feedbacks from "../modules/Feedbacks";
import Contacts from "../modules/Contacts";
import Catalog from "../modules/Catalog";
import Skills from "../modules/Skills";
import Experience from "../modules/Experience";
import Education from "../modules/Education";
import AboutMe from "../modules/AboutMe";


function ProfilePage() {
  return (
    <>
      <AboutMe />
      <Education />
      <Experience />
      <Skills />
      <Catalog />
      <Contacts />
      <Feedbacks />
      <br />
    </>
  );
}

export default ProfilePage;
