import React from "react";

import Greeting from "../modules/Greeting";

function HomePage() {
  return <Greeting />;
}

export default HomePage;
