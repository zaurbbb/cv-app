import React from "react";
import { Link } from "react-router-dom";
import PhotoBox from "../ui/PhotoBox";

import Avatar from "/images/greetingAvatar.png";

function GreetingModule() {
  return (
    <section className="greeting">
      <PhotoBox
        block="greeting"
        name="John Doe"
        title="Programmer. Creative. Innovator"
        description="Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque"
        avatar={Avatar}
      />
      <Link to="/profile">
        <button className="button">
          Know more
        </button>
      </Link>
    </section>
  );
}

export default GreetingModule;
