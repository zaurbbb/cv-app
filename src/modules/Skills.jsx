import React, {
  useEffect,
  useState,
} from "react";

import {
  useDispatch,
  useSelector,
} from "react-redux";

import Title from "../components/Title";
import AddSkill from "../components/AddSkill";
import { fetchSkills } from "../redux";

function Skills() {
  // constants
  const dispatch = useDispatch();
  const { skills } = useSelector((state) => state.skill.skills);

  // states
  const [openEdit, setOpenEdit] = useState(false);

  // constants
  useEffect(() => {
    dispatch(fetchSkills());
  }, [dispatch]);

  // handlers
  const handleEdit = () => {
    setOpenEdit(!openEdit);
  };

  // display
  const displaySkills = skills ? skills.map((skill) => (
    <div
      key={skill.id}
      className="skills__item"
      style={{ width: `${skill.range}%` }}
    >
      <span className="skills__text">
        {skill.name}
      </span>
    </div>
  )) : null;

  return (
    <section className="skills">
      <Title
        text="Skills"
        button
        handleClick={handleEdit}
      />

      {openEdit && <AddSkill />}

      <div className="skills__diagram">
        <div className="skills__wrapper">
          {displaySkills}
        </div>
        <div className="skills__roadmap">
          <div className="skills__level"></div>
          <div className="skills__level"></div>
          <div className="skills__level"></div>
        </div>
        <div className="skills__roadmap">
          <div className="skills__grade">Beginner</div>
          <div className="skills__grade">Proficient</div>
          <div className="skills__grade">Expert</div>
          <div className="skills__grade">Master</div>
        </div>
      </div>
    </section>
  );
}

export default Skills;
