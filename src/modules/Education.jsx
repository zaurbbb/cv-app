import React, { useEffect } from "react";

import {
  useDispatch,
  useSelector,
} from "react-redux";

import { fetchEducations } from "../redux";

import Title from "../components/Title";
import TimeLine from "../components/TimeLine";

function Education() {
  // constants
  const dispatch = useDispatch();
  const educationState = useSelector((state) => state.education);

  // fetch data
  useEffect(() => {
    dispatch(fetchEducations());
  }, [dispatch]);

  return (
    <section id="education">
      <Title text="Education" />

      <TimeLine state={educationState} />
    </section>
  );
}

export default Education;
