import React from "react";

import Portfolio from "../components/Portfolio";
import Title from "../components/Title";

function Catalog() {
  return (
    <section id="catalog">
      <Title text="Portfolio" />
      <Portfolio />
    </section>
  );
}

export default Catalog;
