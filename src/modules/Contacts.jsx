import React from "react";
import Address from "../components/Address";
import Title from "../components/Title";

function Contacts() {
  return (
    <section id="contacts">
      <Title text="Contacts" />
      <Address />
    </section>
  );
}

export default Contacts;
