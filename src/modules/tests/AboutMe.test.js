import { screen } from "@testing-library/react";

import { renderTestApp } from "../../helpers/renderTestApp";

describe("modules/AboutMe tests", () => {
  test("successfully renders the title of section", () => {
    // axios.get.mockReturnValue(response);
    renderTestApp({
      route: "/profile",
      initialState: {},
    });

    const sectionTitle = screen.getByTestId("box-title");
    expect(sectionTitle).toHaveTextContent(/About Me/i);
  });
});

