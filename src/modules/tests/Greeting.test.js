import { screen } from "@testing-library/react";

import { renderTestApp } from "../../helpers/renderTestApp";

describe("modules/AboutMe tests", () => {
  test("successfully renders the title of section", () => {
    // axios.get.mockReturnValue(response);
    renderTestApp({
      route: "/",
      initialState: {},
    });

    const buttonText = screen.getByText(/Know more/i);
    expect(buttonText).toBeInTheDocument();

  });
});

